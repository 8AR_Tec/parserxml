#include "Wrapp.h"

//#include <afxwin.h>


using namespace MSXML2;
//using namespace std;

//Macro to verify memory allcation.
#define CHK_ALLOC(p)        do { if (!(p)) { hr = E_OUTOFMEMORY; goto CleanUp; } } while(0)
// Macro that calls a COM method returning HRESULT value.
#define CHK_HR(stmt)        do { hr=(stmt); if (FAILED(hr)) goto CleanUp; } while(0)

Wrapp::Wrapp() {
}

Wrapp::~Wrapp() {
}

CString Wrapp::GetXML() {
	if (IsValid())
		return (LPCSTR)docPtr->Getxml();
	else
		return _T("");
}

BOOL Wrapp::IsValid() {
	if (DOMNodePtr == NULL)
		return FALSE;
	if (DOMNodePtr.GetInterfacePtr() == NULL)
		return FALSE;
	return TRUE;
}

CString Wrapp::GetValueOfAttribute(LPCTSTR valueName, MSXML2::IXMLDOMNodePtr DOMNodePtr) {
	if (!IsValid())
		return "";

	MSXML2::IXMLDOMNodePtr attribute = DOMNodePtr->Getattributes()->getNamedItem(valueName);
	if (attribute)
	{
		return (LPCSTR)attribute->Gettext();
	}
	return "";
}

MSXML2::IXMLDOMNode* Wrapp::GetPrevSibling() {
	if (!IsValid())
		return NULL;
	return DOMNodePtr->GetpreviousSibling().Detach();
}

MSXML2::IXMLDOMNode* Wrapp::GetNextSibling() {
	if (!IsValid())
		return NULL;
	return DOMNodePtr->GetnextSibling().Detach();
}

inline void TESTHR(HRESULT _hr) {
	if FAILED(_hr) throw(_hr);
}

HRESULT Wrapp::VariantFromString(PCWSTR wszValue, VARIANT &Variant) {
	HRESULT hr = S_OK;
	BSTR bstr = SysAllocString(wszValue);
	CHK_ALLOC(bstr);

	V_VT(&Variant) = VT_BSTR;
	V_BSTR(&Variant) = bstr;

CleanUp:
	return hr;
}

void Wrapp::Init() {
	// init
	TESTHR(CoInitialize(NULL));
	TESTHR(docPtr.CreateInstance("Msxml2.DOMDocument.6.0"));
	VariantInit(&save);
}

void Wrapp::LoadXml() {
	Init();

	// load a document
	_variant_t varXml("sms.xml");    //<----Inaczej
	_variant_t varOut((bool)TRUE);
	varOut = docPtr->load(varXml);
	if ((bool)varOut == FALSE)
		throw(0);
}

void Wrapp::WriteXmlDocument() {
	wcout << _bstr_t(docPtr->xml);
}

void Wrapp::SaveXmlToFile(PCWSTR nameOfFile) {
	//saving
	(LPCSTR)docPtr->get_xml(&bstrXML);
	VariantFromString(nameOfFile, save);
	docPtr->save(save);
}

long Wrapp::NumNodes() {
	if (IsValid())
	{
		return DOMNodePtr->GetchildNodes()->Getlength();
	}
	else
		return 0;
}

MSXML2::IXMLDOMNodePtr Wrapp::GetNode(LPCTSTR strFindText) {
	nodeListPtr = docPtr->getElementsByTagName(strFindText);
	MSXML2::IXMLDOMNodePtr TempPtr = nodeListPtr->Getitem(0);
	return TempPtr;
}

void Wrapp::CreateEmptyDOM(bool &retflag) {
	retflag = true;
	TESTHR(CoInitialize(NULL));
	hr = docPtr.CreateInstance(__uuidof(MSXML2::DOMDocument60), NULL, CLSCTX_INPROC_SERVER);
	if (FAILED(hr)) {
		printf("Failed to instantiate an XML DOM.\n");
		return;
	}
	docPtr->async = VARIANT_FALSE;
	docPtr->validateOnParse = VARIANT_FALSE;
	docPtr->resolveExternals = VARIANT_FALSE;
	docPtr->preserveWhiteSpace = VARIANT_TRUE;
	retflag = false;
}

void Wrapp::ColectPhoneNumbers() {

	BSTR strFindText = L"addrs";
	LoadXml();

	try
	{
		nodeListPtr = docPtr->getElementsByTagName(strFindText);

		for (int i = 0; i < nodeListPtr->length; i++)
		{
			DOMNodePtr = nodeListPtr->Getitem(i);
			MSXML2::IXMLDOMNodePtr newOne = DOMNodePtr->GetlastChild();
			if (DOMNodePtr)
			{
				BSTR bstrItemText = DOMNodePtr->Gettext();
				CString bstrItemAttribute = GetValueOfAttribute(L"address", newOne);


				phoneNumbers.push_back(bstrItemAttribute);
			}
		}
	}
	catch (...)
	{
		cout << "Exception occurred";
	}
}


//colect nodes by tagname
void Wrapp::ColectSmsText() {

	BSTR strFindText = L"parts";
	LoadXml();

	try
	{
		nodeListPtr = docPtr->getElementsByTagName(strFindText);

		for (int i = 0; i < nodeListPtr->length; i++)
		{
			DOMNodePtr = nodeListPtr->Getitem(i);
			MSXML2::IXMLDOMNodePtr newOne = DOMNodePtr->GetlastChild();
			if (DOMNodePtr)
			{
				BSTR bstrItemText = DOMNodePtr->Gettext();
				CString bstrItemAttribute = GetValueOfAttribute(L"text", newOne);

				text.push_back(bstrItemAttribute);
				::MessageBox(NULL, bstrItemAttribute, phoneNumbers[i], MB_OK);
			}
		}
	}
	catch (...)
	{
		cout << "Exception occurred";
	}
}
