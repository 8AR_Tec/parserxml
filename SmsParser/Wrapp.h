#include <tchar.h>
#import <msxml6.dll>
#include <atlstr.h>
#include <string>
#include <stdio.h>
#include <vector>
#include <iostream>

using namespace MSXML2;
using namespace std;
class Wrapp
{
public:
	HRESULT hr{ S_OK };
	MSXML2::IXMLDOMDocumentPtr docPtr;
	MSXML2::IXMLDOMNodePtr DOMNodePtr;
	MSXML2::IXMLDOMNodeListPtr nodeListPtr;
	MSXML2::IXMLDOMNamedNodeMapPtr nodeMapPtr;


	BSTR bstrXML = NULL;
	BSTR strFindText;
	VARIANT save;
	PCWSTR nameOfFile;

	vector<CString> phoneNumbers;
	vector<CString> text;

	//methods
	Wrapp();
	~Wrapp();
	HRESULT VariantFromString(PCWSTR wszValue, VARIANT & Variant);
	void Init();
	void LoadXml();
	void WriteXmlDocument();
	void SaveXmlToFile(PCWSTR nameOfFile);
	long NumNodes();
	MSXML2::IXMLDOMNodePtr GetNode(LPCTSTR strFindText);
	CString GetXML();
	BOOL IsValid();
	void CreateEmptyDOM(bool &retflag);
	void ColectPhoneNumbers();
	void ColectSmsText();
	CString GetValueOfAttribute(LPCTSTR valueName, MSXML2::IXMLDOMNodePtr DOMNodePtr);
	MSXML2::IXMLDOMNode * GetPrevSibling();
	MSXML2::IXMLDOMNode * GetNextSibling();

};

#pragma once

